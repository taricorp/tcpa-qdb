from django.conf.urls import patterns, include, url

from qdb.views import QuoteList, QuoteView, Search

urlpatterns = patterns('',
    url(r'^$', QuoteList.as_view(), name='index'),
    url(r'^(?P<pk>\d+)$', QuoteView.as_view(), name='view-quote'),
    url(r'^search$', Search.as_view(), name='search'),
)
