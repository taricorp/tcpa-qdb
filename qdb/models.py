from django.db import models

class Quote(models.Model):
    created = models.DateTimeField()
    updated = models.DateTimeField(null=True)

    nick = models.CharField(max_length=16)
    hostname = models.CharField(max_length=64)
    content = models.TextField()

    class Meta:
        ordering = ['-created']

    @property
    def lines(self):
        start = 0
        i = 0
        q = self.content
        while i >= 0:
            i = q.find('|', i)
            if i > 0:
                if q[i - 1] != '\\':
                    yield q[start:i].strip().replace('\\|', '|')
                    start = i = i + 1
                else:
                    i += 1
        yield q[start:].strip().replace('\\|', '|')

    @lines.setter
    def lines(self, value):
        self.content = '|'.join(value)
