from functools import partial
from django.test import TestCase
from qdb.models import Quote

Quote = partial(Quote, nick='test')

class QuoteTestCase(TestCase):
    def test_split(self):
        q = Quote(content='Foo|Bar|Baz')
        self.assertEqual(list(q.lines), ['Foo', 'Bar', 'Baz'])

    def test_strip_whitespace(self):
        q = Quote(content='Foo | Bar | Baz')
        self.assertEqual(list(q.lines), ['Foo', 'Bar', 'Baz'])
    
    def test_handle_escape(self):
        q = Quote(content=r'Foo\|Bar|Baz')
        self.assertEqual(list(q.lines), ['Foo|Bar', 'Baz'])

    def test_strip_escape(self):
        q = Quote(content=r'I guess !qsay is broken :\|')
        self.assertEqual(list(q.lines), ['I guess !qsay is broken :|'])
        
