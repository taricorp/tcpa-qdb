from django.shortcuts import redirect
from django.views.generic import ListView, DetailView

from qdb.models import Quote

class Search(ListView):
    context_object_name = 'quotes'
    paginate_by = 20
    template_name = 'qdb/search.html'

    def get(self, *args, **kwargs):
        self.q = self.request.GET.get('q')
        if not self.q:
            return redirect('/')
        else:
            return super(Search, self).get(*args, **kwargs)

    def get_queryset(self):
        self.q = self.request.GET.get('q')
        return Quote.objects.filter(content__icontains=self.q)

    def get_context_data(self, **kwargs):
        context = super(Search, self).get_context_data(**kwargs)
        context['query'] = self.q
        return context

class QuoteList(ListView):
    context_object_name = 'quotes'
    model = Quote
    paginate_by = 50

    # TODO allow admins to view inactive quotes
    #def get_queryset(self):
    #    if self.request.user.is_admin(): return Quote.objects.all()
    #    return self.queryset

class QuoteView(DetailView):
    context_object_name = 'quote'
    model = Quote
