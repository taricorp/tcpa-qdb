import sys
import yaml

from django.db import transaction
from qdb.models import Quote

def main(src):
    with open(src, 'rb') as f:
        data = yaml.safe_load(f)
    assert data['quotes']['columns'] == \
        ['id', 'nick', 'hostname', 'content', 'created_at', 'updated_at']

    with transaction.atomic():
        for raw in data['quotes']['records']:
            (id, nick, hostname, content, created_at, updated_at) = raw
            print(id)
            q = Quote(id=id,
                      created=created_at,
                      updated=updated_at,
                      nick=nick,
                      hostname=hostname,
                      content=content)
            q.save()

if __name__ == '__main__':
    main(sys.argv[1])
